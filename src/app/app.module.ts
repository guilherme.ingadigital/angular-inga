import { FuncionarioService } from './funcionario/funcionario.service';
import { ROUTES } from './app.routes'

import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FuncionarioComponent } from './funcionario/funcionario.component';
import { HomeComponent } from './home/home.component';
import { SplashComponent } from './splash/splash.component';
import { FooterComponent } from './footer/footer.component';
import { MenuComponent } from './menu/menu.component';
import { ListaFuncionariosComponent } from './lista-funcionarios/lista-funcionarios.component';
import { SobreComponent } from '../../../projeto-inga/src/app/sobre/sobre.component';
import { Http } from '@angular/http';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FuncionarioComponent,
    HomeComponent,
    SplashComponent,
    FooterComponent,
    MenuComponent,
    ListaFuncionariosComponent,
    SobreComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [
    Http,
    FuncionarioService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
