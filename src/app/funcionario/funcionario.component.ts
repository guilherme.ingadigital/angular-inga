import { Component, OnInit, Input } from '@angular/core';
import { Funcionario } from './funcionario';
import { Cargo } from '../cargo/cargo';
import { Setor } from '../setor/setor';

@Component({
  selector: 'inga-funcionario',
  templateUrl: './funcionario.component.html',
  styleUrls: ['./funcionario.component.css']
})
export class FuncionarioComponent implements OnInit {

  cargos: Cargo[] = [
    {codigo: 1, descricao: "CEO"},
    {codigo: 2, descricao: "Programador"},
    {codigo: 3, descricao: "Suporte"},
    {codigo: 4, descricao: "Comercial"},
  ]

  setores: Setor[] = [
    {codigo: 1, descricao: "Administrativo"},
    {codigo: 2, descricao: "Desenvolvimento"},
    {codigo: 3, descricao: "Atendimento"}
  ]

  constructor() { }

  @Input() funcionario: Funcionario

  ngOnInit() {
  }

  mudarStatus() {
    this.funcionario.estaViajando = !this.funcionario.estaViajando;
  }

}
