import { Injectable } from "@angular/core";
import { Funcionario } from "./funcionario";

@Injectable()
export class FuncionarioService {

    listFuncionarios: Funcionario[] = [
        {
            nome: "Funcionário 1", 
            estaViajando: false, 
            cargo: {
                codigo: 1,
                descricao: "CEO"
            }
        },
        {
            nome: "Funcionário 2", 
            estaViajando: true, 
            cidadeAtual: "Corbélia",
            cargo: {
                codigo: 2,
                descricao: "Programador"
            }
        },
        {
            nome: "Funcionário 3", 
            estaViajando: true, 
            cidadeAtual: "Corbélia",
            cargo: {
                codigo: 3,
                descricao: "Analista de Suporte"
            }
        }
    ];

    constructor(){

    }

    getFuncionarios(){
        // Esse método faria uma requisição para uma API e retornaria os funcionários.
        // this.http.get();
        return this.listFuncionarios; 
    }
}