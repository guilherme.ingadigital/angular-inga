import { SobreComponent } from './../../../projeto-inga/src/app/sobre/sobre.component';
import { Routes } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { ListaFuncionariosComponent } from "./lista-funcionarios/lista-funcionarios.component";

export const ROUTES: Routes = [
    {path: '', component: HomeComponent},
    {path: 'funcionarios', component: ListaFuncionariosComponent},
    {path: 'sobre', component: SobreComponent},
];