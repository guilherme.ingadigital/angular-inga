import { FuncionarioService } from './../funcionario/funcionario.service';
import { Component, OnInit } from '@angular/core';
import { Funcionario } from '../funcionario/funcionario';

@Component({
  selector: 'inga-lista-funcionarios',
  templateUrl: './lista-funcionarios.component.html',
  styleUrls: ['./lista-funcionarios.component.css']
})
export class ListaFuncionariosComponent implements OnInit {

  constructor(public funcionarioService: FuncionarioService) { }
  
  funcionarios: Funcionario[];
  
  ngOnInit() {
    this.funcionarios = this.funcionarioService.getFuncionarios();
  }

}
